from flask import render_template, request, Flask, redirect, url_for
from app import app
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Email, Length
from flask_bootstrap import Bootstrap
from flask_sqlalchemy  import SQLAlchemy
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user

#from .forms import PostData

#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://localhost/planner'
#app.secret_key = 'Development Key'
#app = Flask(__name__)
#app.run(debug=True)
bootstrap = Bootstrap(app)
app.config['SECRET_KEY'] = 'superdupersecretkey'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/domhackl/uni/webdev/project2/famstories/app/users.db'
db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

class Users(UserMixin, db.Model):
        id = db.Column(db.Integer, primary_key = True)
        username = db.Column(db.String(15), unique = True)
        email = db.Column(db.String(30), unique = True)
        password = db.Column(db.String(80), unique = True)

@login_manager.user_loader
def load_user(user_id):
    return Users.query.get(int(user_id))



class LoginForm(FlaskForm):
        username = StringField("Username", validators=[InputRequired(), Length(min=4, max=18)])
        password = PasswordField("Password", validators=[InputRequired(), Length(min=8, max=80)])

class RegisterForm(FlaskForm):
    email = StringField('email', validators=[InputRequired(), Email(message='Not valid email'), Length(max=30)])
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])

@app.route('/')
@login_required
def index():
    return render_template('index.html',
                            title='Home Page ting',
                            name=current_user.username )


@app.route('/logout', methods=['GET','POST'])
@login_required
def logout():
    logout_user
    return redirect(url_for('loggedout'))


@app.route('/loggedout', methods=['GET','POST'])
@login_required
def loggedout():
    return render_template('loggedout.html')


@app.route('/login', methods=['GET','POST'])
def login():
    form = LoginForm()

    if form.validate_on_submit():
            user = Users.query.filter_by(username = form.username.data).first()
            if user:
                if user.password == form.password.data:
                        login_user(user)
                        return redirect(url_for('index'))

            return '<h1>Incorrect Password or username</h1>'
    user = {'name': 'Dom Hackl',
            'age': 20,
            'nationality': 'Hungarian'}
       

    return render_template('login.html',
                            title='Log into to use the shop',
                            form=form
                            )



@app.route('/signup', methods=['GET','POST'])
def register():
    form = RegisterForm()

    if form.validate_on_submit():
            #return '<h2>'+ form.username.data + ' ' + form.email.data + ' ' +  form.password.data + '</h2>'
            new_user =  Users(username = form.username.data, email = form.email.data, password = form.password.data)
            db.session.add(new_user)
            db.session.commit()
            return '<h1>You have successfully registered!</h1>' 

    return render_template('signup.html',
                            title='Sign Up to use the shop',
                            form=form
                            )
    
@app.route('/friends')    
def people():
    people = ["Adam", "Kieran", "John", "Mike"]
    return render_template('people.html',
                            people=people)

                            
@app.route('/header')
def header():
        user = {'name': 'Dom Hackl',
                'age': 20,
                'nationality': 'Hungarian'}
        tabs = ["Add Element", "View All Entries", "View Uncompleted Entries", "View Completed Entries"]
        return render_template('header.html',
                                title='View Lists',
                                user=user,
                                tabs=tabs)
                           
@app.route('/submitted', methods=['GET','POST'])
def addToDo():
        user = {'name': 'Dom Hackl'}
        tabs = ["Add Element", "View All Entries", "View Uncompleted Entries", "View Completed Entries"]
        #form = PostData()
        #if form.validate_on_submit():
         #       flash('Successfully received form data. The title is %s and the description is %s = %s'%(form.title.data, form.desc.data))
        #if request.method == 'POST':
         #       title = request.form['noteTitle']
          #      desc = request.form['noteDetail']
        form = AddNoteForm()
        if request.method == 'POST':
                if form.validate() == False:
                        render_template('submitted.html', form=form)
                else:
                        return "success"
        elif request.method == 'GET':
                return render_template('submitted.html',
                        user=user,
                        tabs=tabs,
                        title='Add a New Note',
                        form=form)

       

